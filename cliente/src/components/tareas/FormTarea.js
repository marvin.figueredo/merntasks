import React, {useContext, useState, useEffect} from 'react';
import proyectoContext from '../../context/proyectos/proyectoContext';
import tareaContext from '../../context/tareas/tareaContext';

const FormTarea = () => {

    const proyectosContext = useContext(proyectoContext);
    const { proyecto } = proyectosContext;

    const tareasContext = useContext(tareaContext);
    const { tareaseleccionada, errortarea, agregarTarea, validarTarea, 
        obtenerTareas, actualizarTarea, limpiarTarea } = tareasContext;

    // state del formulario
    const [tarea, guardarTarea]= useState({
        nombre: ''
    })

    // Effect que detecta si hay una tarea seleccionada
    useEffect(() => {
        if (tareaseleccionada !== null){
            guardarTarea(tareaseleccionada);
        }
        else{
            guardarTarea({
                nombre: ''
            })
        }
    }, [tareaseleccionada])

    if (!proyecto) return null;

    // proyecto actual
    const [proyectoActual] = proyecto;

    // Leer los valores del formulario
    const handleChange = e => {
        guardarTarea({
            ...tarea,
            [e.target.name]: e.target.value
        })
    }

    // extraer el nombre de la tarea
    const { nombre } = tarea;

    const onSubmit = e => {
        e.preventDefault();

        // validar
        if(nombre.trim() === ''){
            validarTarea();
            return;
        }

        // Si es edición o nueva tarea
        if (tareaseleccionada === null){
            // agregar tarea al state
            tarea.proyecto = proyectoActual._id;
            agregarTarea(tarea);
        }
        else{
            // actualizar tarea existente
            actualizarTarea(tarea);
            limpiarTarea();
        }
        

        // reiniciar el form
        guardarTarea({
            nombre: ''
        })

        // refrescar las tareas
        obtenerTareas(proyectoActual._id);
    }

    return ( 
        <div className="formulario">
            <form
                onSubmit={onSubmit}
            >
                <div className="contenedor-input">
                    <input
                        type="text"
                        className="input-text"
                        placeholder="Nombre Tarea..."
                        name="nombre"
                        value={nombre}
                        onChange={handleChange}
                    />
                </div>
                <div className="contenedor-input">
                    <input
                        type="submit"
                        className="btn btn-primario btn-submit btn-block"
                        value={tareaseleccionada ? 'Editar Tarea' : 'Agregar Tarea'}
                    />
                </div>
            </form>
            {errortarea ? <p className="mensaje error">El nombre de la tarea es obligatorio</p> : null}
        </div>
);
}
 
export default FormTarea;